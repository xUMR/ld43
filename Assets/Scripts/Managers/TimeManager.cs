using UnityEngine;

public class TimeManager : MonoBehaviour
{
    [SerializeField] private float _timeScale = 1;

#if UNITY_EDITOR
    [NaughtyAttributes.Button]
#endif
    public void CurrentTİme() => print(Time.time);

#if UNITY_EDITOR
    [NaughtyAttributes.Button]
#endif
    public void Pause() => _timeScale = 0;

#if UNITY_EDITOR
    [NaughtyAttributes.Button]
#endif
    public void UnPause() => _timeScale = 1;

    private void Awake()
    {
        _timeScale = Time.timeScale;
    }

    private void Update()
    {
        Time.timeScale = _timeScale;
    }
}
