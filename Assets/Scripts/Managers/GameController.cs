using System;
using Umrty.Core;
using Umrty.MagicStrings;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;

#if UNITY_EDITOR
[ExecutionOrder(-1)]
#endif
public class GameController : MonoBehaviour
{
    [SerializeField] private Fader _fader;
    [SerializeField] private TextDisplayer _endGameTextDisplayer;
    [SerializeField] private TextDisplayer _instructionsTextDisplayer;

    private void Awake()
    {
        CheckEndScene();

        PrepareGameStartEvents();
        PrepareGameOverEvents();

        Events.Instance.OnGameStart.OnNext(Unit.Default);

        Events.Instance.OnDeath
            .Where(target => target.CompareTag(Tags.Player))
            .Subscribe(target => Events.Instance.OnGameOver.OnNext("Don't die."))
            .AddTo(this);
    }

    private void CheckEndScene()
    {
        Events.Instance.OnGameComplete
            .Delay(TimeSpan.FromSeconds(13))
            .SubscribeWithState(this,
                (_, self) =>
                {
                    self._fader.FadeTo(Color.black, 3);
                    self._endGameTextDisplayer.Display("That's all folks", 1.25f, 3, 3);
                    LeanTween.delayedCall(6, () => SceneManager.LoadScene(Scenes.Menu));
                })
            .AddTo(this);

        SceneManager.sceneLoaded += (scene, mode) =>
        {
            if (scene.name != Scenes.Single2) return;

            Events.Instance.OnGameComplete.OnNext(Unit.Default);
        };
    }

    private void PrepareGameStartEvents()
    {
        var gameStart = Events.Instance.OnGameStart;

        gameStart.SubscribeWithState(this, (_, self) =>
            {
                self._fader.FadeTo(Color.black, 0);
                self._fader.FadeTo(Color.clear, 1);
            })
            .AddTo(this);
    }

    private void PrepareGameOverEvents()
    {
        var gameOver = Events.Instance.OnGameOver;

        gameOver.SubscribeWithState(this, (_, self) => self._fader.FadeTo(Color.black, 1)).AddTo(this);

        gameOver.Delay(TimeSpan.FromSeconds(1))
            .SubscribeWithState(this, (_, self) =>
            {
                self._endGameTextDisplayer.Display("GAME OVER", 1.25f, 3);

                InputManager.AxisStream[InputAxes.RestartLevel]
                    .DistinctUntilChanged()
                    .Where(axis => axis > 0)
                    .Subscribe(__ => UnityUtility.RestartScene())
                    .AddTo(self);
            })
            .AddTo(this);

        gameOver.Delay(TimeSpan.FromSeconds(2))
            .Subscribe(_ => Events.Instance.OnNextInstruction.OnNext("Press 'R' to restart."))
            .AddTo(this);
    }
}
