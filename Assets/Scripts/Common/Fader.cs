using UnityEngine;
using UnityEngine.UI;

public class Fader : MonoBehaviour
{
    [SerializeField] private Image _image;

    private int _fadeId;

    private void Awake()
    {
        _image = _image ? _image : GetComponentInChildren<Image>();
    }

    public void FadeTo(Color to, float duration) => _fadeId = LeanTween.color(_image.rectTransform, to, duration).id;

    public void Cancel() => LeanTween.cancel(_fadeId);
}
