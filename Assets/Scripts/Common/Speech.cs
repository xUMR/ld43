using UnityEngine;
using UnityEngine.UI;

public class Speech : MonoBehaviour
{
    [SerializeField] private Text _text;

    public void Display(string message, Color color, float duration = 1, float delay = 0)
    {
        _text.color = Color.clear;
        _text.text = message;

        var targetScale = _text.rectTransform.localScale * .95f;
        _text.rectTransform.LeanScale(targetScale, duration).setDelay(delay);

        var targetY = _text.rectTransform.position.y + duration;

        _text.rectTransform.LeanMoveLocalY(targetY, duration).setDelay(delay);
        _text.rectTransform.LeanColorText(color, .1f).setDelay(delay);
        _text.rectTransform.LeanAlphaText(0, .1f).setDelay(duration + delay);

        Destroy(gameObject, duration + delay + .5f);
    }
}
