using Umrty.Core;
using Umrty.Core.Extensions;
using UniRx;
using UnityEngine;

#if UNITY_EDITOR
[ExecutionOrder(-2)]
#endif
public class Events : MonoBehaviour
{
    #region Singleton

    private static Events _instance;

    public static Events Instance
    {
        get
        {
            if (_instance != null)
                return _instance;

            UnityUtility.EnsureSingleInstance(ref _instance).DontDestroyOnLoad();
            return _instance;
        }
    }

    #endregion

    public ISubject<Transform> OnCharacterSelect { get; private set; }
    public ISubject<PositionTime> OnPlayerShoot { get; private set; }
    public ISubject<Vector3> OnEnemyHear { get; private set; }
    public ISubject<Transform> OnDeath { get; private set; }

    public ISubject<Transform> OnCameraFollow { get; private set; }
    public ISubject<Transform> OnCameraUnFollow { get; private set; }

    public ISubject<Unit> OnGameStart { get; private set; }
    public ISubject<string> OnGameOver { get; private set; }
    public ISubject<Unit> OnNextSceneAvailable { get; private set; }

    public ISubject<string> OnNextInstruction { get; private set; }

    public ISubject<Unit> OnGameComplete { get; private set; }

    private void Awake()
    {
        OnCharacterSelect = new Subject<Transform>();
        OnPlayerShoot = new Subject<PositionTime>();
        OnEnemyHear = new Subject<Vector3>();
        OnDeath = new Subject<Transform>();

        OnCameraFollow = new Subject<Transform>();
        OnCameraUnFollow = new Subject<Transform>();

        OnGameStart = new Subject<Unit>();
        OnGameOver = new Subject<string>();
        OnNextSceneAvailable = new Subject<Unit>();

        OnNextInstruction = new Subject<string>();

        OnGameComplete = new Subject<Unit>();
    }
}
