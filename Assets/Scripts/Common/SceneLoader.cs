using Umrty.Core;
using Umrty.MagicStrings;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneLoader : MonoBehaviour
{
    [SerializeField] private Collider2D _collider;
    [SerializeField] private SpriteRenderer _sprite;

#if UNITY_EDITOR
    [NaughtyAttributes.Dropdown(nameof(_sceneList))]
#endif
    [SerializeField]
    private string _sceneToLoadOnTrigger;

    private readonly string[] _sceneList =
        {Scenes.Single0, Scenes.Single1, Scenes.Single2, Scenes.Menu, Scenes.Tutorial};

    private bool _nextSceneIsLocked = true;

    private void Awake()
    {
        _collider = _collider ? _collider : GetComponent<Collider2D>();

        Events.Instance.OnNextSceneAvailable
            .SubscribeWithState(this, (_, self) =>
            {
                self._nextSceneIsLocked = false;
                self._sprite.gameObject.LeanAlpha(.5f, .2f);
            })
            .AddTo(this);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (_nextSceneIsLocked) return;

        var otherRoot = other.transform.root;
        if (otherRoot.CompareTag(Tags.Player))
        {
            Load(_sceneToLoadOnTrigger);
        }
    }

    public void Load(string sceneName) => SceneManager.LoadScene(sceneName);

    public void Restart() => UnityUtility.RestartScene();
}
