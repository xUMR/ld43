using System;
using System.Collections.Generic;
using System.Linq;
using Umrty.Core;
using Umrty.Core.Extensions;
using Umrty.MagicStrings;
using UniRx;
using UnityEngine;

public class CameraFollowMulti : CameraFollow
{
    [SerializeField] private List<Transform> _targets;
    [SerializeField] private float _playerBias = 1.25f;
    [SerializeField] private bool _isCoop;

    private bool _playerBiasApplicable;

    private Func<Vector3, Transform, Vector3> _sumPositions;

    private void Awake()
    {
        Events.Instance.OnCameraFollow.SubscribeWithState(this, (target, self) => self.Follow(target)).AddTo(this);
        Events.Instance.OnCameraUnFollow.SubscribeWithState(this, (target, self) => self.UnFollow(target)).AddTo(this);

        Events.Instance.OnCharacterSelect
                .Subscribe(target => Events.Instance.OnCameraFollow.OnNext(target))
//            .SubscribeWithState(this, (target, self) => self._targets.remo = target)
            .AddTo(this);

        _sumPositions = SumPositions;
    }

    private void Update()
    {
        var positionsSum =
            _targets.Aggregate(Vector3.zero, _sumPositions);

        var targetPosition = positionsSum / _targets.Count;
        TargetPosition = targetPosition;
    }

    private Vector3 SumPositions(Vector3 sum, Transform target)
    {
        if (target == null) return sum;

//        var multiplier = _playerBiasApplicable && target.CompareTag(Tags.Player) ? _playerBias : 1;
        var multiplier = 1;
        return sum + multiplier * target.position;
    }

    public void Follow(Transform target)
    {
//        if (!target.CompareTag(Tags.Player))
//            _playerBiasApplicable = true;
//        else
//            _playerBiasApplicable = !_targets.Any(t => t.CompareTag(Tags.Player));

        if (!_isCoop && target.CompareTag(Tags.Player))
        {
            var previousPlayerTransform = _targets.FirstOrDefault(t => t.CompareTag(Tags.Player));

            UnFollow(previousPlayerTransform);
        }

        _targets.AddIfNotInList(target);
    }

    public void UnFollow(Transform target)
    {
        if (target == null) return;

//        _playerBiasApplicable = !_targets.Any(t => t.CompareTag(Tags.Player));
        _targets.Remove(target);
    }
}
