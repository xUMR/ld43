using UnityEngine;

public struct PositionTime
{
    public readonly Vector2 Position;
    public readonly float Time;

    public PositionTime(Vector2 position) : this(position, UnityEngine.Time.time) { }

    public PositionTime(Vector2 position, float time)
    {
        Position = position;
        Time = time;
    }

    public override string ToString() => $"{{Position={Position}, Time={Time}}}";
}
