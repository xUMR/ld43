using System.Linq;
using UnityEngine;

public class SpeechDisplayer : MonoBehaviour
{
    [SerializeField] private Speech _prefab;
    [SerializeField] private Color _color;

    [SerializeField] private string[] _messagesOnStart;
    [SerializeField] private float[] _durationsOnStart;
    [SerializeField] private float[] _delaysOnStart;

    private void Start()
    {
        float totalDuration = 0;
        for (var i = 0; i < _messagesOnStart.Length; i++)
        {
            var message = _messagesOnStart[i];
            var duration = _durationsOnStart[i];
            var delay = _delaysOnStart[i] + totalDuration;
            totalDuration = duration;
            totalDuration += delay;

            Display(message, duration, delay);
        }
    }

    public void Display(string message, float duration = 1, float delay = 0, Transform parent = null)
    {
        parent = parent ? parent : transform;
        var speech = Instantiate(_prefab, parent);
        speech.Display(message, _color, duration, delay);
    }

#if UNITY_EDITOR
    [NaughtyAttributes.Button]
#endif
    private void PrintTotalDuration() => print(_durationsOnStart.Sum() + _delaysOnStart.Sum());
}
