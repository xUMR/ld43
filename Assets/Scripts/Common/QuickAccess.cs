using Umrty.MagicStrings;
using UnityEngine;

public static class QuickAccess
{
    public static LayerMask NonPlayer = Layers.EnemyMask | Layers.VisionBlockerMask;
    public static LayerMask NonEnemy = Layers.PlayerMask | Layers.VisionBlockerMask;
}
