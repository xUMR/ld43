using UnityEngine;

[CreateAssetMenu(fileName = "CoopConfig", menuName = "SO/CoopConfig")]
public class CoopConfig : ScriptableObject
{
    public enum InputType
    {
        KeyboardMouse,
        ControllerAll,
        Controller1,
        Controller2
    }

    public InputType Player1;
    public InputType Player2;
}
