using System;
using Umrty.Core;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    [SerializeField] private CameraFollow _follow;

    private void Awake()
    {
        _follow = GetComponent<CameraFollow>();
    }

    public void Shake(Vector3 direction, float duration, float multiplier = 1)
    {
        var to = transform.position + direction.normalized * multiplier;
        LeanTween.move(gameObject, to, duration)
            .setEaseShake()
            .setLoopOnce();
    }

#if UNITY_EDITOR
    [NaughtyAttributes.Button]
#endif
    private void TestShake() => Shake(Vector3.left, .1f, .1f);
}
