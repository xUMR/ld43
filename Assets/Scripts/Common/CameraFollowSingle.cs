using Umrty.Core;
using UniRx;
using UnityEngine;

public class CameraFollowSingle : CameraFollow
{
    [SerializeField] private Transform _target;

    private void Awake()
    {
        Events.Instance.OnCharacterSelect
            .SubscribeWithState(this, (target, self) => self._target = target)
            .AddTo(this);
    }

    private void Update()
    {
        TargetPosition = _target.position;
    }
}
