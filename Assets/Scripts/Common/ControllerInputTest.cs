using Umrty.Core;
using Umrty.MagicStrings;
using UnityEngine;

public class ControllerInputTest : MonoBehaviour
{
    public Vector2 Move;
    public Vector2 Move1;
    public Vector2 Move2;

    public Vector2 Look;
    public Vector2 Look1;
    public Vector2 Look2;

    public float Fire;
    public float Fire1;
    public float Fire2;

    public float SwapCharacter;

    public float TargetLock;

    public float RestartLevel;

    public float Cancel;
    public float Submit;

    private void Update()
    {
        Fire = Input.GetAxisRaw(InputAxes.Fire);
        Fire1 = Input.GetAxisRaw(InputAxes.Fire1);
        Fire2 = Input.GetAxisRaw(InputAxes.Fire2);

        Look = InputManager.GetVector(InputAxes.LookX, InputAxes.LookY);
        Look1 = InputManager.GetVector(InputAxes.LookX1, InputAxes.LookY1);
        Look2 = InputManager.GetVector(InputAxes.LookX2, InputAxes.LookY2);

        Move = InputManager.GetVector(InputAxes.Horizontal, InputAxes.Vertical);
        Move1 = InputManager.GetVector(InputAxes.Horizontal1, InputAxes.Vertical1);
        Move2 = InputManager.GetVector(InputAxes.Horizontal2, InputAxes.Vertical2);

        SwapCharacter = Input.GetAxisRaw(InputAxes.SwapCharacter);
        TargetLock = Input.GetAxisRaw(InputAxes.TargetLock);
        RestartLevel = Input.GetAxisRaw(InputAxes.RestartLevel);

        Cancel = Input.GetAxisRaw(InputAxes.Cancel);
        Submit= Input.GetAxisRaw(InputAxes.Submit);
    }
}
