using Umrty.Core;
using Umrty.MagicStrings;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

public class EnemyRadar : MonoBehaviour
{
    [SerializeField] private Aim _aim;
    [SerializeField] private float _range = 10;
    [SerializeField] private EnemyLookAround _lookAround;

    private int _requiredRaycastCheckFails = 5;
    private int _raycastCheckFails = 0;

    private Transform _root;

    private void Awake()
    {
        _aim = _aim ? _aim : transform.root.GetComponentInChildren<Aim>();
        _root = transform.root;
        _lookAround = _root.GetComponentInChildren<EnemyLookAround>();
    }

    private void Start()
    {
//        var onTriggerEnter = this.OnTriggerEnter2DAsObservable();

        this.OnTriggerEnter2DAsObservable()
            .Where(colliderOther => colliderOther.CompareTag(Tags.Player))
            .SubscribeWithState(this, (colliderOther, self) => self.TriggerEnterPlayer(colliderOther))
            .AddTo(this);

        this.OnTriggerEnter2DAsObservable()
            .Where(colliderOther => colliderOther.CompareTag(Tags.DeadEnemy))
            .SubscribeWithState(this, (colliderOther, self) => self.TriggerEnterDeadEnemy(colliderOther))
            .AddTo(this);

        this.OnTriggerExit2DAsObservable()
            .Where(colliderOther => colliderOther.CompareTag(Tags.Player))
            .SubscribeWithState(this, (colliderOther, self) =>
            {
                if (self._aim.Target == colliderOther.transform)
                {
                    self._aim.Target = null;
                    self._lookAround.enabled = true;
                }
            }).AddTo(this);

        this.OnTriggerStay2DAsObservable()
            .Where(colliderOther => colliderOther.CompareTag(Tags.Player))
            .SubscribeWithState(this, (colliderOther, self) =>
            {
                if (self._aim.Target == null) return;

//                print("running trigger stay");

                var direction = colliderOther.transform.position - self._root.position;
                var hits = Physics2D.RaycastNonAlloc(self._root.position, direction, Arrays<RaycastHit2D>.Single,
                    self._range, QuickAccess.NonEnemy);

                if (hits < 1) return;

                if (Arrays<RaycastHit2D>.Single[0].transform.root == colliderOther.transform.root)
                {
//                    print("Player still in range.");
//                    self._aim.Target = colliderOther.transform;
                }
                else if (self._raycastCheckFails >= self._requiredRaycastCheckFails)
                {
                    self._aim.Target = null;
                    self._lookAround.enabled = true;
                }
                else
                {
                    self._raycastCheckFails++;
                }
            }).AddTo(this);
    }

    private void TriggerEnterPlayer(Collider2D colliderOther)
    {
        var direction = colliderOther.transform.position - _root.position;
        var hits = Physics2D.RaycastNonAlloc(_root.position, direction, Arrays<RaycastHit2D>.Single,
            _range, QuickAccess.NonEnemy);

        if (hits < 1) return;

        if (Arrays<RaycastHit2D>.Single[0].transform == colliderOther.transform)
        {
            _aim.Target = colliderOther.transform;
            _raycastCheckFails = 0;
            _lookAround.enabled = false;
        }
    }

    private void TriggerEnterDeadEnemy(Collider2D colliderOther)
    {
        var origin = _root.position + _root.right / 2;
        var direction = colliderOther.transform.position - origin;
        var hits = Physics2D.RaycastNonAlloc(origin, direction, Arrays<RaycastHit2D>.Single,
            _range, QuickAccess.NonPlayer);

        Debug.DrawRay(origin, direction, Color.cyan, 1f);
        if (hits < 1) return;

        if (Arrays<RaycastHit2D>.Single[0].transform == colliderOther.transform)
        {
            Events.Instance.OnGameOver.OnNext("Don't let them see the bodies.");
        }
    }
}
