using Umrty.Core;
using UniRx;
using UnityEngine;

public class CrouchController : MonoBehaviour
{
    [SerializeField] private bool _isCrouched;

    public bool IsCrouched => _isCrouched;

    private void Awake()
    {
        InputManager.KeyStream[KeyCode.LeftControl]
            .Where(keyState => keyState == KeyState.IsDown)
            .SubscribeWithState(this, (_, self) => self._isCrouched = !self._isCrouched)
            .AddTo(this);
    }
}
