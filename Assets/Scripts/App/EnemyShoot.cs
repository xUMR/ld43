using System;
using System.Collections;
using UniRx;
using UnityEngine;

public class EnemyShoot : MonoBehaviour
{
    [SerializeField] private Aim _aim;
    [SerializeField] private float _shootDelay;
    [SerializeField] private ShootController _shootController;

    private IEnumerator _coroutine;

    private Transform _root;

    private void Awake()
    {
        _root = transform.root;
        _aim = _aim ? _aim : _root.GetComponentInChildren<Aim>();
        _shootController = _shootController == null
            ? _root.GetComponentInChildren<ShootController>()
            : _shootController;

        _aim.ObserveEveryValueChanged(aim => aim.Target)
            .Where(target => target != null)
            .Delay(TimeSpan.FromSeconds(_shootDelay))
            .SubscribeWithState(this, (target, self) => self._shootController.Shoot())
            .AddTo(this);
    }
}
