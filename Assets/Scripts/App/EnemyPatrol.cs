using System;
using System.Collections.Generic;
using Umrty.Core.Extensions;
using UniRx;
using UnityEngine;
using static PatrolRoute;

public class EnemyPatrol : MonoBehaviour
{
    [SerializeField] private PatrolRoute _patrolRoute;
    [SerializeField] private PatrolMode _patrolMode;
    [SerializeField] private int _currentIndex;
    [SerializeField] private int _nextIndex;
    [SerializeField] private float _movementSpeed;
    [SerializeField] private float _rotationSpeed;
    [SerializeField] private Transform _targetTransform;
    [SerializeField] private bool _wait;
    [SerializeField] private float _alertDuration = 3;

    private IEnumerator<int> _indicesEnumerator;

    private IReadOnlyList<Vector3> Waypoints => _patrolRoute.Waypoints;

    private void Start()
    {
        _indicesEnumerator = _patrolRoute.Indices(_patrolMode);
        _currentIndex = _indicesEnumerator.Next();
        _nextIndex = _currentIndex;

        UpdateIndices();

        var enemyHear = Events.Instance.OnEnemyHear;

        enemyHear
            .SubscribeWithState(this, (_, self) => self._wait = true)
            .AddTo(this);

        enemyHear
            .Throttle(TimeSpan.FromSeconds(_alertDuration))
            .SubscribeWithState(this, (_, self) => self.MoveToNextWaypoint())
            .AddTo(this);
    }

    private void UpdateIndices()
    {
        _currentIndex = _nextIndex;
        _nextIndex = _indicesEnumerator.Next();
    }

    private void Update()
    {
        if (!_wait)
            _targetTransform.Translate(_targetTransform.right * _movementSpeed * Time.deltaTime, Space.World);

        if (ArrivedAtNextWaypoint())
        {
            UpdateIndices();
            MoveToNextWaypoint();
        }

    }

    private bool ArrivedAtNextWaypoint() =>
        Vector3.SqrMagnitude(Waypoints[_nextIndex] - _targetTransform.position) < .1f;

    private void MoveToNextWaypoint()
    {
        _wait = true;

        var angle = _targetTransform.Angle(Waypoints[_nextIndex]).SimplifyAngle();
        var time = Mathf.Abs(angle / _rotationSpeed);
        _targetTransform
            .LeanRotateAround(Vector3.forward, angle, time)
            .setEaseInOutQuad()
            .setOnComplete(patrol =>
            {
                var ep = (EnemyPatrol) patrol;
                ep._wait = false;
            }, this);
    }
}
