using Umrty.Core;
using Umrty.Core.Extensions;
using Umrty.MagicStrings;
using UniRx;
using UnityEngine;

public class ControllerAim : MonoBehaviour
{
    [SerializeField] private Transform _targetTransform;

    private void Awake()
    {
        InputManager.Axis2DStream[InputAxes.LookX, InputAxes.LookY]
            .Where(v => !v.Approx(Vector2.zero))
            .Select(v => v.normalized)
            .SubscribeWithState(this, (v, self) =>
            {
                if (!self.enabled) return;

                var angle = Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
                self._targetTransform.rotation = Quaternion.Euler(0, 0, angle);
            })
            .AddTo(this);
    }
}
