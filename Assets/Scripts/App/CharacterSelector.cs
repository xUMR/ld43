using System.Collections.Generic;
using Umrty.Core;
using Umrty.MagicStrings;
using UniRx;
using UnityEngine;

public class CharacterSelector : MonoBehaviour
{
    [SerializeField] private List<Transform> _characters;
    [SerializeField] private int _selectedCharacterIndex;
    public Transform SelectedCharacter => _characters[_selectedCharacterIndex];

    private void Awake()
    {
        InputManager.AxisStream[InputAxes.SwapCharacter]
            .DistinctUntilChanged()
            .Where(keyState => keyState > 0)
            .SubscribeWithState(this, (_, self) => self.NextCharacter())
            .AddTo(this);
    }

    private void Start()
    {
        NextCharacter();
        NextCharacter();
    }

    private void NextCharacter()
    {
        var prevIndex = _selectedCharacterIndex;
        _selectedCharacterIndex = (_selectedCharacterIndex + 1) % _characters.Count;

        if (prevIndex == _selectedCharacterIndex)
            return;

		ToggleComponents(_characters[prevIndex], false);
		ToggleComponents(SelectedCharacter, true);

        Events.Instance.OnCharacterSelect.OnNext(SelectedCharacter);
    }

    private void ToggleComponents(Component t, bool flag)
    {
        t.GetComponentInChildren<CharacterController2D>().enabled = flag;
        t.GetComponentInChildren<ShootController>().enabled = flag;
        t.GetComponentInChildren<ControllerAim>().enabled = flag;
        t.GetComponentInChildren<TargetLocker>().enabled = flag;
        t.GetComponentInChildren<Aim>().enabled = flag;
    }
}
