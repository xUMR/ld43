using Umrty.Core.Extensions;
using UniRx;
using UnityEngine;

public class CharacterDie : MonoBehaviour
{
    [SerializeField] private ParticleSystem _particleSystem;
    [SerializeField] private GameObject _keepAfterDeath;

#if UNITY_EDITOR
    [NaughtyAttributes.Button]
#endif
    private void Emit()
    {
        _particleSystem.gameObject.SetActive(true);
        _particleSystem.Play();
    }

    private void Awake()
    {
        _particleSystem = _particleSystem == null
            ? transform.root.GetComponentInChildren<ParticleSystem>(true)
            : _particleSystem;

        Events.Instance.OnDeath
            .SubscribeWithState(this, (e, self) =>
            {
                if (e != self.transform.root) return;

                self.Emit();
                self._keepAfterDeath.SetActive(true);
                foreach (var go in self.transform.root.GetChildrenGameObjectsEnumerable())
                {
                    if (go != self._keepAfterDeath)
                        Destroy(go);
                }
            })
            .AddTo(this);
    }
}
