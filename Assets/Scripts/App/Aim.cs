using Umrty.Core.Extensions;
using UnityEngine;

public class Aim : MonoBehaviour
{
    [SerializeField] private Transform _target;
    [SerializeField] private bool _isPlayer;
    private Transform _root;

    public Transform Target
    {
        get => _target;
        set => _target = value;
    }

    public Vector3 TargetPosition => _target ? _target.position : MouseWorldPosition;

    public static Vector3 MouseWorldPosition => Camera.main.ScreenPointToRay(Input.mousePosition).origin.WithZ(0);

    private void Awake()
    {
        _root = transform.root;
    }

    private void LookAtTransform(Transform other) => _root.right = other.position - _root.position;

    private void LookAtMouse()
    {
        var dir = Input.mousePosition - Camera.main.WorldToScreenPoint(_root.position);
        var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        _root.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }

    private void Update()
    {
        if (Target)
            LookAtTransform(Target);
        else if (_isPlayer)
            LookAtMouse();
    }
}
