using System.Collections.Generic;
using Umrty.Core;
using Umrty.MagicStrings;
using UniRx;
using UnityEngine;

public class TargetLocker : MonoBehaviour
{
    [SerializeField] private List<Transform> _targets;
    [SerializeField] private Aim _aim;
    [SerializeField] private Collider2D _range;

    private Transform _root;
    private CharacterSelector _selector;

    private void Awake()
    {
        _root = transform.root;
        _aim = _aim ? _aim : _root.GetComponentInChildren<Aim>();

        InputManager.AxisStream[InputAxes.TargetLock]
            .DistinctUntilChanged()
            .Where(keyState => keyState > 0)
            .SubscribeWithState(this, (_, self) =>
            {
                if (!self.enabled) return;
                self.Toggle();
            })
            .AddTo(this);

        Events.Instance.OnDeath
            .SubscribeWithState(this, (target, self) =>
            {
                if (!self.enabled) return;
                if (self._aim.Target == target)
                    self.Unlock();
            })
            .AddTo(this);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
//        if (!other.gameObject.name.Contains(Tags.Enemy))
//            return;

        var otherRoot = other.transform.root;
        if (otherRoot.CompareTag(Tags.Enemy) && otherRoot.childCount > 1)
        {
            _targets.Add(otherRoot);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
//        if (!other.gameObject.name.Contains(Tags.Enemy))
//            return;

        var otherRoot = other.transform.root;
        if (otherRoot.CompareTag(Tags.Enemy))
        {
            _targets.Remove(otherRoot);
        }
    }

    private void FixedUpdate()
    {
        transform.rotation = Quaternion.identity;
    }

    public void Toggle()
    {
        if (_aim.Target == null)
            LockClosest();
        else
            Unlock();
    }

#if UNITY_EDITOR
    [NaughtyAttributes.Button]
#endif
    public void LockClosest()
    {
        var currentDistance = float.PositiveInfinity;
        Transform currentClosest = null;
        foreach (var target in _targets)
        {
            var distance = Vector3.SqrMagnitude(target.position - _root.position);
            if (distance > currentDistance) continue;

            currentClosest = target;
            currentDistance = distance;
        }

        if (currentClosest == null)
            return;

        _aim.Target = currentClosest;
        Events.Instance.OnCameraFollow.OnNext(_aim.Target);
    }

#if UNITY_EDITOR
    [NaughtyAttributes.Button]
#endif
    public void Unlock()
    {
        Events.Instance.OnCameraUnFollow.OnNext(_aim.Target);
        _aim.Target = null;
    }
}
