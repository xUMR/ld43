using System;
using System.Collections;
using Umrty.Core;
using Umrty.Core.Extensions;
using Umrty.MagicStrings;
using UniRx;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class AudioIndicator : MonoBehaviour
{
    [SerializeField] private float _maxAudioMaskDuration;
    [SerializeField] private Image _image;
    [SerializeField] private AudioClip _clip;
    [SerializeField] private AudioMask _audioMask;
    [SerializeField] private AudioMixerGroup _mixer;
    private AudioPlayer _player;

    public float TimeDifference;
    private float _lastBeatStart;
    private float _lastBeatEnd;

    private bool _displayIndicator;

    private AudioIndicator _instance;

    private void Awake()
    {
        this.EnsureSingleInstance(ref _instance).DontDestroyOnLoad();

        var onEnemyHear = Events.Instance.OnEnemyHear
            .ThrottleFirst(TimeSpan.FromSeconds(.2));

        onEnemyHear
            .Where(_ => SceneManager.GetActiveScene().name != Scenes.Tutorial)
            .Subscribe(_ => Events.Instance.OnGameOver.OnNext("Don't let them hear you."))
            .AddTo(this);

        onEnemyHear
//            .Where(_ => SceneManager.GetActiveScene().name == Scenes.Tutorial)
            .First(_ => SceneManager.GetActiveScene().name == Scenes.Tutorial)
            .Subscribe(_ => Events.Instance.OnNextInstruction.OnNext("Don't worry, this is the tutorial."))
            .AddTo(this);
    }

    private void Start()
    {
        SceneManager.sceneLoaded += (scene, mode) =>
        {
            if (scene.name == Scenes.Menu || scene.name == Scenes.Single2)
            {
                Destroy(_instance.gameObject);
            }
        };

        _player = AudioManager.Instance.Prepare(_clip);
        _player.Loop = true;
        _player.LoopStartEnd = Vector2.up * 2;
        _player.Stop();

        var waitEarly = new WaitForSeconds(.5f - _maxAudioMaskDuration);
        var waitLate = new WaitForSeconds(.5f + _maxAudioMaskDuration);
        var waitRepeat = new WaitForSeconds(.5f);

        StartCoroutine(DoCoroutine(waitRepeat, waitRepeat, PlayTrack));
        StartCoroutine(DoCoroutine(waitEarly, waitRepeat, MaskAudio));
        StartCoroutine(DoCoroutine(waitLate, waitRepeat, UnMaskAudio));

        StartCoroutine(DoCoroutine(waitEarly, waitRepeat, 16, DisplayIndicator));
        StartCoroutine(DoCoroutine(waitLate, waitRepeat, 16, HideIndicator));
    }

    private IEnumerator DoCoroutine(WaitForSeconds delay, WaitForSeconds repeatRate, System.Action action)
    {
        yield return delay;

        while (true)
        {
            action();
            yield return repeatRate;
        }
    }

    private IEnumerator DoCoroutine(WaitForSeconds delay, WaitForSeconds repeatRate, int times, System.Action action)
    {
        yield return delay;

        for (var i = 0; i < times; i++)
        {
            action();
            yield return repeatRate;
        }
    }

    private void MaskAudio()
    {
        _lastBeatStart = Time.time;
        _audioMask.IsAudioMasked = true;
    }

    private void UnMaskAudio()
    {
        _lastBeatEnd = Time.time;
        TimeDifference = _lastBeatEnd - _lastBeatStart;
        _audioMask.IsAudioMasked = false;
    }

    private void DisplayIndicator() => _image.color = Color.white;

    private void HideIndicator() => _image.color = Color.clear;

    private void PlayTrack() => AudioManager.Instance.Prepare(_clip, _mixer).Play();
}
