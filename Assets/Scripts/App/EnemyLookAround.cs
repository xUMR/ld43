using System;
using Umrty.Core.Extensions;
using UniRx;
using UnityEngine;

public class EnemyLookAround : MonoBehaviour
{
    [SerializeField] private float _angle;
    [SerializeField] private float _speed;
    [SerializeField] private float _alertDuration = 3;
    [SerializeField] private float _initialAngle;

    private int _tweenId;
    private bool _isLookingAround;

    private void Start()
    {
        var enemyHear = Events.Instance.OnEnemyHear;

        enemyHear
            .SubscribeWithState(this, (_, self) => self.Stop())
            .AddTo(this);

        enemyHear
            .Throttle(TimeSpan.FromSeconds(_alertDuration))
            .SubscribeWithState(this, (_, self) => self.FixOrientation())
            .AddTo(this);

        _initialAngle = transform.root.eulerAngles.z;
    }

    private void FixOrientation()
    {
        if (_isLookingAround) return;

        var angle = (_initialAngle - transform.root.eulerAngles.z).SimplifyAngle();
        var time = Mathf.Abs(angle / _speed);
        _tweenId = transform.root
            .LeanRotateAround(Vector3.forward, angle, time)
            .setEaseInOutQuad()
            .setOnComplete(_ =>
            {
                var e = (EnemyLookAround) _;
                e._isLookingAround = false;
                e.Begin();
            }, this)
            .id;

        _isLookingAround = true;
    }

    private void Begin()
    {
        if (_isLookingAround) return;

        var time = Mathf.Abs(_angle / _speed);
        _tweenId = transform.root
            .LeanRotateAround(Vector3.forward, _angle, time)
            .setEaseInOutQuad()
            .setLoopPingPong()
            .id;

        _isLookingAround = true;
    }

    private void Stop()
    {
        if (!_isLookingAround) return;

        LeanTween.cancel(_tweenId);
        _isLookingAround = false;
    }

    private void OnEnable() => Begin();

    private void OnDisable() => Stop();
}
