using System;
using System.Collections.Generic;
using System.Linq;
using Umrty.Core.Extensions;
using UnityEngine;

public class PatrolRoute : MonoBehaviour
{
    public enum PatrolMode
    {
        Loop,
        PingPong
    }

    [SerializeField] private List<Vector3> _waypoints;

#if UNITY_EDITOR
    [NaughtyAttributes.ReadOnly]
#endif
    [SerializeField]
    private int[] _indices;

    public IReadOnlyList<Vector3> Waypoints => _waypoints;

    private void Awake()
    {
        BuildWaypoints(false);
    }

    public IEnumerator<int> Indices(PatrolMode patrolMode) => IndicesInner(patrolMode).GetEnumerator();

    private IEnumerable<int> IndicesInner(PatrolMode patrolMode)
    {
        switch (patrolMode)
        {
            case PatrolMode.Loop: return _indices.Loop();
            case PatrolMode.PingPong: return _indices.PingPong();
            default: throw new ArgumentOutOfRangeException(nameof(patrolMode), patrolMode, null);
        }
    }

    private void BuildWaypoints(bool warnIfNotBuilt)
    {
        if (_waypoints != null && _waypoints.Count > 0 &&
            _indices != null && _indices.Length == _waypoints.Count) return;

        if (warnIfNotBuilt)
            Debug.LogWarning("Patrol route not built.");

        BuildRoute();
    }

#if UNITY_EDITOR
    [NaughtyAttributes.Button]
#endif
    private void BuildRoute()
    {
        _waypoints = transform.GetChildren().Select(t => t.position).ToList();
        _indices = Enumerable.Range(0, _waypoints.Count).ToArray();
    }

#if UNITY_EDITOR
    [NaughtyAttributes.Button]
    private void DisableChildren() =>
        transform.GetChildrenEnumerable().ToList().ForEach(t => t.gameObject.SetActive(false));
#endif

#if UNITY_EDITOR
    [NaughtyAttributes.Button]
    private void ClearChildren() =>
        transform.GetChildrenEnumerable().ToList().ForEach(t => DestroyImmediate(t.gameObject));
#endif
}
