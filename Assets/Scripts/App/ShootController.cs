using System;
using System.Collections;
using Umrty.Core;
using Umrty.Core.Extensions;
using Umrty.MagicStrings;
using UniRx;
using UnityEngine;
using Random = UnityEngine.Random;

public class ShootController : MonoBehaviour
{
    [SerializeField] private float _angle;
    [SerializeField] private float _distance;
    [SerializeField] private LayerMask _layerMask;
    [SerializeField] private Aim _aim;
    [SerializeField] private CameraShake _cameraShake;
    [SerializeField] private float _shakeMultiplier = .025f;
    [SerializeField] private float _lineCastOriginDistanceMultiplier = .5f;
    [SerializeField] private LineRenderer _lineRenderer;
    [SerializeField] private bool _isPlayer;
    [SerializeField] private AudioClip _clip;

    private RaycastHit2D[] _results;

    private IEnumerator _coroutine;

    private Transform _root;

    private void Awake()
    {
        _root = transform.root;
        _results = Arrays<RaycastHit2D>.Single;
        _aim = _aim ? _aim : _root.GetComponentInChildren<Aim>();
        _lineRenderer = _lineRenderer ? _lineRenderer : _root.GetComponentInChildren<LineRenderer>();
        _cameraShake = FindObjectOfType<CameraShake>();

        InputManager.AxisStream[InputAxes.Fire]
            .DistinctUntilChanged()
            .TakeUntil(Events.Instance.OnGameOver)
            .TakeUntil(Events.Instance.OnNextSceneAvailable)
            .Where(axis => axis > 0)
            .SubscribeWithState(this, (_, self) =>
            {
                if (self.enabled && self._isPlayer)
                    self.Shoot();
            })
            .AddTo(this);

        Events.Instance.OnEnemyHear
            .ThrottleFirst(TimeSpan.FromSeconds(.1))
            .SubscribeWithState(this, (_, self) => AudioManager.Instance.Prepare(self._clip).Play())
            .AddTo(this);
    }

#if UNITY_EDITOR
    [NaughtyAttributes.Button]
#endif
    public void Shoot()
    {
        var origin = _root.position + _root.right * _lineCastOriginDistanceMultiplier;

        var angle = Random.Range(-_angle / 2, _angle / 2);
        var direction = _root.right.RotateAround(_root.position, angle);

//        Debug.DrawRay(origin, direction * _distance, Color.yellow, .1f);

        _cameraShake.Shake(-direction, .1f, _shakeMultiplier);

        if (_root.CompareTag(Tags.Player))
            Events.Instance.OnPlayerShoot.OnNext(new PositionTime(origin));

        var hits = Physics2D.RaycastNonAlloc(origin, direction, Arrays<RaycastHit2D>.Single, _distance, _layerMask);
        if (hits <= 0) return;

        var otherRoot = Arrays<RaycastHit2D>.Single[0].transform.root;
        var destination = Arrays<RaycastHit2D>.Single[0].point;
        DrawLine(origin, destination);

        if (otherRoot.CompareTag(Tags.Enemy))
        {
            Events.Instance.OnDeath.OnNext(otherRoot);
        }
        else if (otherRoot.CompareTag(Tags.Player))
        {
            if (_isPlayer)
                Events.Instance.OnNextInstruction.OnNext("Friendly fire is ON.");
            Events.Instance.OnDeath.OnNext(otherRoot);
        }
    }

    private void DrawLine(Vector3 origin, Vector3 destination, float duration = .05f)
    {
        _lineRenderer.SetPosition(0, origin);
        _lineRenderer.SetPosition(1, destination);

        _lineRenderer.enabled = true;

        if (_coroutine != null)
            StopCoroutine(_coroutine);

        _coroutine = DisableLineRendererCoroutine(duration);
        StartCoroutine(_coroutine);
    }

    private IEnumerator DisableLineRendererCoroutine(float delay)
    {
        var waitUntil = Time.time + delay;
        while (Time.time < waitUntil)
            yield return null;

        _lineRenderer.enabled = false;
    }
}
