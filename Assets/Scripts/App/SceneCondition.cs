using System.Collections.Generic;
using System.Linq;
using Umrty.MagicStrings;
using UniRx;
using UnityEngine;

public class SceneCondition : MonoBehaviour
{
    [SerializeField] private List<GameObject> _enemies;

    private void Start()
    {
        if (!_enemies.Any()) Events.Instance.OnNextSceneAvailable.OnNext(Unit.Default);
    }

#if UNITY_EDITOR
    [NaughtyAttributes.Button]
#endif
    private List<GameObject> FetchAllEnemies()
    {
        _enemies = GameObject.FindGameObjectsWithTag(Tags.Enemy).Where(go => go.name.Contains(Tags.Enemy)).ToList();
        return _enemies;
    }

    private void Awake()
    {
        _enemies = _enemies == null || _enemies.Count == 0 ? FetchAllEnemies() : _enemies;

        Events.Instance.OnDeath
            .Where(t => t.CompareTag(Tags.Enemy))
            .SubscribeWithState(this, (t, self) =>
            {
                self._enemies.Remove(t.gameObject);
                if (self._enemies.Count == 0)
                    Events.Instance.OnNextSceneAvailable.OnNext(Unit.Default);
            })
            .AddTo(this);

    }
}
