using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class TextDisplayer : MonoBehaviour
{
    [SerializeField] private CanvasGroup _canvasGroup;
    [SerializeField] private Text _text;

    [SerializeField] private string[] _messagesOnStart;
    [SerializeField] private float[] _durationsOnStart;

    [SerializeField] private bool _displayInstructions;

    private Queue<(string message, float duration)> _messageQueue;
    private bool _isBusy;

    private void Awake()
    {
        _canvasGroup = _canvasGroup ? _canvasGroup : GetComponentInChildren<CanvasGroup>();
        _text = _text ? _text : GetComponentInChildren<Text>();

        _canvasGroup.alpha = 0;

        if (!_displayInstructions)
            return;

        _messageQueue = new Queue<(string message, float duration)>();

        Events.Instance.OnNextInstruction
            .SubscribeWithState(this, (message, self) => self._messageQueue.Enqueue((message, 2)))
            .AddTo(this);

        Events.Instance.OnGameOver
            .Subscribe(message => Events.Instance.OnNextInstruction.OnNext(message))
            .AddTo(this);
    }

    private void Start()
    {
        for (var i = 0; i < _messagesOnStart.Length; i++)
        {
            var message = _messagesOnStart[i];
            var duration = _durationsOnStart[i];

            _messageQueue.Enqueue((message, duration));
        }
    }

    private void Update()
    {
        if (!_displayInstructions) return;
        if (!_messageQueue.Any()) return;
        if (_isBusy) return;

        var item = _messageQueue.Dequeue();
        StartCoroutine(DequeueCoroutine(item.message, item.duration));
    }

    private IEnumerator DequeueCoroutine(string message, float duration)
    {
        _isBusy = true;

        _canvasGroup.alpha = 0;
        _text.text = message;

        _canvasGroup.LeanAlpha(1, .1f);
        _canvasGroup.LeanAlpha(0, .1f).setDelay(duration);

        var waitUntil = Time.time + duration;
        while (Time.time < waitUntil)
            yield return null;

        _isBusy = false;
    }

    public void Display(string message, float scale, float duration, float delay)
    {
        _text.text = message;
        _text.rectTransform.LeanScale(Vector3.one * scale, duration)
            .setDelay(delay)
            .setOnComplete(rt => ((RectTransform) rt).localScale = Vector3.one, _text.rectTransform);

        _canvasGroup.LeanAlpha(1, .1f).setDelay(delay);
        _canvasGroup.LeanAlpha(0, .1f).setDelay(delay + duration - .1f);
    }

    public void Display(string message, float scale, float duration)
    {
        _text.text = message;
        _text.rectTransform.LeanScale(Vector3.one * scale, duration)
            .setOnComplete(rt => ((RectTransform) rt).localScale = Vector3.one, _text.rectTransform);

        _canvasGroup.LeanAlpha(1, .1f);
        _canvasGroup.LeanAlpha(0, .1f).setDelay(duration - .1f);
    }

    public void Display(string message, float duration)
    {
        _messageQueue.Enqueue((message, duration));
    }

    public void Display(string message)
    {
        _text.text = message;
        _canvasGroup.LeanAlpha(1, .1f);
    }

#if UNITY_EDITOR
    [NaughtyAttributes.Button]
#endif
    private void DisplayTest() => Display("HELLO, WORLD!", 3);
}
