using System;
using Umrty.Core.Extensions;
using UniRx;
using UnityEngine;

public class EnemyHear : MonoBehaviour
{
    [SerializeField] private float _reactionTime = .2f;
    [SerializeField] private float _reactionDelay = .1f;
    [SerializeField] private float _alwaysAudibleSqrRange = 5f;
    [SerializeField] private float _speed = 600;
    [SerializeField] private AudioMask _audioMask;

    private Vector3 _lastGunshot;

    private Transform _root;

    private void Awake()
    {
        _root = transform.root;
        _audioMask = FindObjectOfType<AudioMask>();

        Func<PositionTime, bool> _gunshotIsAudible = pt => !_audioMask.IsAudioMasked;

        var playerShoot = Events.Instance.OnPlayerShoot;

        playerShoot.SubscribeWithState(this, (pt, self) => self._lastGunshot = pt.Position).AddTo(this);

        playerShoot
            .Where(_gunshotIsAudible)
            .Subscribe(pt => Events.Instance.OnEnemyHear.OnNext(pt.Position))
            .AddTo(this);

        Events.Instance.OnEnemyHear
            .ThrottleFirst(TimeSpan.FromSeconds(_reactionTime))
            .Delay(TimeSpan.FromSeconds(_reactionDelay))
            .SubscribeWithState(this, (target, self) => self.LookAt(target))
            .AddTo(this);

        Events.Instance.OnDeath
            .SubscribeWithState(this, (target, self) =>
            {
                if (target == self._root) return;
                if ((target.position - self._root.position).sqrMagnitude < self._alwaysAudibleSqrRange)
                {
                    var delay = self.LookAt(target.position);
                    self.LookAt(self._lastGunshot, delay);
                }
            })
            .AddTo(this);
    }

    private float LookAt(Vector3 target, float delay = 0)
    {
        var angle = _root.Angle(target).SimplifyAngle();
        var time = Mathf.Abs(angle / _speed);
        _root.LeanRotateAround(Vector3.forward, angle, time).setDelay(delay);
        return time;
    }

    #if UNITY_EDITOR
    [NaughtyAttributes.Button]
#endif
    private void PrintSqrDistance()
    {
        var player = FindObjectOfType<CrouchController>().transform.root;
        print(Vector3.SqrMagnitude(player.position - transform.root.position));
    }

}
