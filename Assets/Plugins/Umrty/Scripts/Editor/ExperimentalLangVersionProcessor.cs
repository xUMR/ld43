using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;

namespace Umrty.Editor
{
    public class ExperimentalLangVersionProcessor : AssetPostprocessor
    {
        private const string TempFile = "$Temp.csproj";
        private const string Key = "LangVersion";
        private const string TargetLangVersion = "7";

        private static readonly string Pattern = $@"(.*)<{Key}>.+</{Key}>(.*)";
        private static readonly string Replacement = $@"$1<{Key}>{TargetLangVersion}</{Key}>$2";

        private static void OnGeneratedCSProjectFiles()
        {
            var projectPath = UmrtyUtility.Utility.ProjectPath;
            var projectFiles = Directory.GetFiles(projectPath).Where(file => file.EndsWith(".csproj"));
            foreach (var file in projectFiles)
                if (!HasTargetLangVersion(file))
                    SetLangVersionExperimental(file);
        }

        private static void SetLangVersionExperimental(string filename)
        {
            FileUtil.DeleteFileOrDirectory(TempFile);

            using (var writer = new StreamWriter(File.Open(TempFile, FileMode.CreateNew)))
            {
                foreach (var line in File.ReadLines(filename))
                {
                    if (!line.Contains(Key))
                    {
                        writer.WriteLine(line);
                    }
                    else
                    {
                        var modifiedLine = Regex.Replace(line, Pattern, Replacement);
                        writer.WriteLine(modifiedLine);
                    }
                }
            }

            FileUtil.ReplaceFile(TempFile, filename);
            FileUtil.DeleteFileOrDirectory(TempFile);
        }

        private static bool HasTargetLangVersion(string filename)
        {
            var lines = File.ReadLines(filename);
            var langVersion = Regex.Replace(lines.ElementAt(3), $".*<{Key}>(.+)</{Key}>.*", "$1");

            return langVersion == TargetLangVersion;
        }
    }
}
