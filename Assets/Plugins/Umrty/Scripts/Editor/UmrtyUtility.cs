using System.IO;
using System.Linq;
using System.Reflection;

namespace Umrty.Editor
{
    public static class UmrtyUtility
    {
        public static class Utility
        {
            // assume dll is in Library/ScriptAssemblies
            public static DirectoryInfo ProjectDirectory { get; } =
                new DirectoryInfo(Assembly.GetExecutingAssembly().Location).Parent.Parent.Parent;

            public static string ProjectPath { get; } = ProjectDirectory.FullName;

            public static DirectoryInfo PluginDirectory { get; } =
                new DirectoryInfo(Assembly.GetExecutingAssembly().Location).Parent.Parent.Parent
                    .EnumerateDirectories("Assets").First()
                    .EnumerateDirectories("Plugins").First()
                    .EnumerateDirectories("Umrty").First();

            public static string PluginPath { get; } = PluginDirectory.FullName;
        }
    }
}
