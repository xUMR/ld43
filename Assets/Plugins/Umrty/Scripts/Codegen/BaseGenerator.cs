using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Umrty.Core.Extensions;
using UnityEditor;

namespace Umrty.Codegen
{
    public abstract class BaseGenerator
    {
        public static readonly string ProjectSettingsDirectory = "ProjectSettings";

        public static readonly string DestinationDirectory =
            Path.Combine("Assets", "Plugins", "Umrty", "Scripts", "MagicStrings");

        public static readonly string Namespace = "Umrty.MagicStrings";

        private string _class;
        public virtual string Class => _class ?? (_class = DetermineClassName());

        private IList<string> _items;

        protected IList<string> Items => _items ?? (_items = ParseItems().Distinct().ToList());

        protected abstract IEnumerable<string> ParseItems();

        protected abstract IEnumerable<Func<string, string>> VariableGenerators { get; }

        protected virtual IEnumerable<string> RequiredNamespaces { get; } = Enumerable.Empty<string>();

        public static string PublicStaticReadonlyVariable(string type, string field, string value) =>
            $"public static readonly {FieldDeclaration(type, field, value)}";

        public static string PublicConstVariable(string type, string field, string value) =>
            $"public const {FieldDeclaration(type, field, value)}";

        public static string Field(string item) => ObjectNames.NicifyVariableName(item).RemoveWhiteSpace();

        public static string FieldDeclaration(string type, string field, string value) => $"{type} {field} = {value};";

        private string DetermineClassName()
        {
            var name = GetType().Name;
            return name.Substring(0, name.Length - "Generator".Length);
        }

        private static void Indent(TextWriter writer, int n)
        {
            for (var i = 0; i < n; i++)
                writer.Write("    ");
        }

        internal void Generate()
        {
            var source = $"{Class}.cs";
            var destination = Path.Combine(DestinationDirectory, source);

            using (var writer = new StreamWriter(File.Open(source, FileMode.Create)))
            {
                writer.WriteLine("// GENERATED //");
                writer.WriteLine();

                foreach (var ns in RequiredNamespaces.Distinct())
                {
                    writer.WriteLine($"using {ns};");
                }

                if (RequiredNamespaces.Any())
                    writer.WriteLine();

                writer.WriteLine($"namespace {Namespace}");
                writer.WriteLine("{");
                GenerateClass(writer);
                writer.WriteLine("}");
            }

            FileUtil.ReplaceFile(source, destination);
            FileUtil.DeleteFileOrDirectory(source);
        }

        private void GenerateClass(TextWriter writer)
        {
            var indent = 1;

            Indent(writer, indent);
            writer.WriteLine($"public class {Class}");
            Indent(writer, indent);
            writer.WriteLine("{");

            var count = VariableGenerators.Count();

            var itemList = new List<string>(Items.Count);
            var generatedArray = false;

            var i = 0;
            indent++;
            foreach (var generator in VariableGenerators)
            {
                foreach (var item in Items)
                {
                    var variable = generator(item);
                    Indent(writer, indent);
                    writer.WriteLine($"{variable}");

                    if (!generatedArray)
                        itemList.Add($"{Field(item)}");
                }

                // todo only for the first string generator?
                if (!generatedArray && itemList.Any() && itemList[0].Contains("string"))
                {
                    writer.WriteLine();
                    Indent(writer, indent);
                    var allItems = PublicStaticReadonlyVariable("string[]", "AllItems", itemList.AsString("{", "}"));
                    writer.WriteLine(allItems);
                    generatedArray = true;
                }

                if (++i < count)
                    writer.WriteLine();

                itemList.Clear();
            }
            indent--;

            Indent(writer, indent);
            writer.WriteLine("}");
        }
    }
}
