using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Umrty.Core.Extensions;
using UnityEditor;

namespace Umrty.Codegen
{
    public class TagsGenetator : BaseGenerator
    {
        protected override IEnumerable<Func<string, string>> VariableGenerators { get; } =
            new List<Func<string, string>> {GenerateTag};

        // https://docs.unity3d.com/Manual/Tags.html
        private readonly IEnumerable<string> _defaultTags = new List<string>
            {"Untagged", "Respawn", "Finish", "EditorOnly", "MainCamera", "Player", "GameController"};

        private static string GenerateTag(string item) =>
            PublicStaticReadonlyVariable("string", Field(item), $"\"{item}\"");

        protected override IEnumerable<string> ParseItems()
        {
            foreach (var tag in _defaultTags)
                yield return tag;

            var obj = new SerializedObject(
                AssetDatabase.LoadAssetAtPath(Path.Combine(ProjectSettingsDirectory, "TagManager.asset"),
                    typeof(object)));

            var tags = obj.FindProperty("tags");
            for (var i = 0; i < tags.arraySize; i++)
            {
                var tag = tags.GetArrayElementAtIndex(i).stringValue;
                if (!string.IsNullOrWhiteSpace(tag) && !_defaultTags.Contains(tag))
                    yield return tag;
            }
        }
    }
}
