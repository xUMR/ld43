using System;
using System.Collections.Generic;
using System.Linq;
using Umrty.Core.Extensions;
using Umrty.Editor;
using UnityEditor;

namespace Umrty.Codegen
{
    public class ScenesGenerator : BaseGenerator
    {
        protected override IEnumerable<Func<string, string>> VariableGenerators { get; } =
            new List<Func<string, string>> {GenerateScene};

        protected override IEnumerable<string> RequiredNamespaces { get; } =
            new List<string> {typeof(UnityEngine.SceneManagement.SceneManager).Namespace};

        private static string GenerateScene(string item) =>
            PublicStaticReadonlyVariable("string", Field(item), $"\"{item}\"");

        protected override IEnumerable<string> ParseItems()
        {
            var projectDirectory = UmrtyUtility.Utility.ProjectDirectory;
            var scenesDirectory = projectDirectory
                .EnumerateDirectories("Assets").First()
                .EnumerateDirectories("Scenes").First();

            foreach (var file in scenesDirectory.EnumerateFiles("*.unity"))
                yield return file.Name.Substring(0, file.Name.Length - file.Extension.Length);
        }
    }
}
