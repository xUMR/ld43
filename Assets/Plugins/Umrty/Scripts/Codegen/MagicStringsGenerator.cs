using System;
using System.IO;
using System.Linq;
using System.Reflection;
using Umrty.Core.Extensions;
using Umrty.Editor;
using UnityEditor;
using UnityEngine;

namespace Umrty.Codegen
{
    public static class MagicStringsGenerator
    {
        private static readonly string AsmdefContents = "{ \"name\": \"Umrty.MagicStrings\" }";

        private static readonly string AsmdefSource = "Umrty.MagicStrings.asmdef";

        private static readonly string AsmdefDestination =
            Path.Combine("Assets", "Plugins", "Umrty", "Scripts", "MagicStrings", AsmdefSource);

        public static void CreateAsmdefFile()
        {
            using (var writer = new StreamWriter(File.Open(AsmdefSource, FileMode.Create)))
                writer.Write(AsmdefContents);

            FileUtil.ReplaceFile(AsmdefSource, AsmdefDestination);
            FileUtil.DeleteFileOrDirectory(AsmdefSource);
        }

        [MenuItem("Tools/Umrty.MagicStrings/Generate All")]
        public static void GenerateAll()
        {
            CreateAsmdefFile();

            var generatorTypes = Assembly.GetExecutingAssembly().DefinedTypes
                .Where(type => type.IsSubclassOf(typeof(BaseGenerator)));

            foreach (var generatorType in generatorTypes)
                Generate(generatorType);
        }

        private static void Generate(Type generatorType)
        {
            var generator = (BaseGenerator) Activator.CreateInstance(generatorType);
            generator.Generate();
            Debug.Log($"Generated {generator.Class}");
        }
    }
}
