using UnityEngine;

namespace Umrty.Core
{
    [RequireComponent(typeof(Camera))]
    public class CameraFollow : MonoBehaviour
    {
        public Vector3 TargetPosition;
        public Vector3 Offset;
        public float Speed;

        private Vector3 _velocity;

        private void LateUpdate()
        {
            var destination = TargetPosition + Offset;
            var source = transform.position;
            transform.position = Vector3.SmoothDamp(source, destination, ref _velocity, Speed * Time.deltaTime);
        }
    }
}
