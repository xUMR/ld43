using Umrty.Core.Extensions;
using UniRx.Toolkit;
using UnityEngine;
using UnityEngine.Audio;

namespace Umrty.Core
{
    public class AudioManager : MonoBehaviour
    {
        public static AudioManager Instance;

        public AudioMixerGroup MixerGroup;
        public AudioPlayer Prefab;

        private ObjectPool<AudioPlayer> _audioPool;

        private void Awake()
        {
            this.EnsureSingleInstance(ref Instance).DontDestroyOnLoad();

            _audioPool = new AudioPlayerPool(Prefab, transform, MixerGroup);
        }

        public bool IsPlaying(AudioClip clip)
        {
            foreach (var child in transform.GetChildrenEnumerable())
            {
                var player = child.GetComponent<AudioPlayer>();
                if (player == null) continue;
                if (player.IsPlaying && player.Clip == clip)
                {
                    return true;
                }
            }

            return false;
        }

        public AudioPlayer Prepare(AudioClip clip) => Prepare(clip, MixerGroup);

        public AudioPlayer Prepare(AudioClip clip, AudioMixerGroup mixerGroup)
        {
            var player = _audioPool.Rent();
            player.Clip = clip;
            player.Mixer = mixerGroup;

            return player;
        }

        public AudioPlayer GetActivePlayer(AudioClip audioClip) =>
            GetComponentsInChildren<AudioPlayer>(false)
                .FirstOrDefaultWithState(audioClip, (player, clip) => player.Clip == clip);

        public void Pause(AudioPlayer player) => player.Pause();

        public void TransitionToSnapshots(AudioMixerSnapshot[] snapshots, float[] weights, float timeToReach) =>
            MixerGroup.audioMixer.TransitionToSnapshots(snapshots, weights, timeToReach);
    }
}
