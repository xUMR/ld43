using UnityEngine;

namespace Umrty.Core
{
// A WaitForSecondsRealtime instance can't be used more than once, use this instead
// Don't forget to call Reset before yield return, or call it like yield return wait.Reset();
    public class WaitForSecondsUnscaled : CustomYieldInstruction
    {
        private readonly float _duration;
        private float _waitUntil;

        public WaitForSecondsUnscaled(float duration)
        {
            _duration = duration;
            Reset();
        }

        public override bool keepWaiting
        {
            get
            {
                if (Time.unscaledTime < _waitUntil)
                    return true;

                Reset();
                return false;
            }
        }

        public new WaitForSecondsUnscaled Reset()
        {
            _waitUntil = Time.unscaledTime + _duration;
            return this;
        }
    }
}
