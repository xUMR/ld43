using UniRx.Toolkit;
using UnityEngine;
using UnityEngine.Audio;

namespace Umrty.Core
{
    public class AudioPlayerPool : ObjectPool<AudioPlayer>
    {
        private readonly AudioPlayer _prefab;
        private readonly Transform _parent;
        private readonly AudioMixerGroup _mixerGroup;

        public AudioPlayerPool(AudioPlayer prefab, Transform parent, AudioMixerGroup mixerGroup)
        {
            _prefab = prefab;
            _parent = parent;
            _mixerGroup = mixerGroup;
        }

        protected override AudioPlayer CreateInstance()
        {
            var player = Object.Instantiate(_prefab, _parent);
            player.Mixer = _mixerGroup;
            player.Pool = this;

            return player;
        }
    }
}
