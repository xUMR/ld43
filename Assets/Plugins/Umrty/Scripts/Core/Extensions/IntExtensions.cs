// ReSharper disable UnusedMember.Global

namespace Umrty.Core.Extensions
{
    public static class IntExtensions
    {
        #region IsOdd

        public static bool IsOdd(this int n) => (n & 1) == 1;
        public static bool IsOdd(this uint n) => (n & 1) == 1;
        public static bool IsOdd(this short n) => (n & 1) == 1;
        public static bool IsOdd(this ushort n) => (n & 1) == 1;

        #endregion

        #region IsEven

        public static bool IsEven(this int n) => (n & 1) == 0;
        public static bool IsEven(this uint n) => (n & 1) == 0;
        public static bool IsEven(this short n) => (n & 1) == 0;
        public static bool IsEven(this ushort n) => (n & 1) == 0;

        #endregion

        public static T Select<T>(this int n, T whenNegative, T whenZero, T whenPositive)
        {
            if (n == 0) return whenZero;
            return n > 0 ? whenPositive : whenNegative;
        }

        public static float RoundToNearestMultipleOf(this int n, int k)
        {
            var m = n % k;
            if (m < k / 2) return n - m;
            return n + k - m;
        }
    }
}
