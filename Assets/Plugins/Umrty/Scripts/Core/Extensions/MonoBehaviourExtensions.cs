using UnityEngine;

// ReSharper disable UnusedMember.Global

namespace Umrty.Core.Extensions
{
    public static class MonoBehaviourExtensions
    {
        public static T MakeSingleton<T>(this T self, ref T instance) where T : MonoBehaviour
        {
            if (instance == null)
            {
                instance = self;
                Object.DontDestroyOnLoad(self.gameObject);
            }
            else
            {
                Object.Destroy(self.gameObject);
            }

            return self;
        }

        public static T DontDestroyOnLoad<T>(this T self) where T : MonoBehaviour
        {
            Object.DontDestroyOnLoad(self.gameObject);
            return self;
        }

        public static T DestroyOtherInstances<T>(this T self, ref T instance) where T : MonoBehaviour
        {
            var objects = Object.FindObjectsOfType<T>();
            foreach (var o in objects)
                if (instance != o)
                    Object.Destroy(o.gameObject);

            return self;
        }

        public static T EnsureSingleInstance<T>(this T self, ref T instance) where T : MonoBehaviour
        {
            if (instance != null)
                return self.DestroyOtherInstances(ref instance);

            var objects = Object.FindObjectsOfType<T>();
            if (objects.Length == 0)
            {
                var singleton = new GameObject(typeof(T).ToString());
                instance = singleton.AddComponent<T>();
            }
            else
            {
                instance = objects[0];
                for (var i = 1; i < objects.Length; i++)
                    Object.Destroy(objects[i].gameObject);
            }

            return self;
        }
    }
}
