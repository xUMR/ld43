using UnityEngine;

// ReSharper disable UnusedMember.Global

namespace Umrty.Core.Extensions
{
    public static class GameObjectExtensions
    {
        public static T GetOrAddComponent<T>(this GameObject self) where T : Component
        {
            var component = self.GetComponent<T>();
            return component == null ? self.AddComponent<T>() : component;
        }

        public static Transform GetRootTransform(this GameObject self) => self.transform.GetRootTransform();
    }
}
