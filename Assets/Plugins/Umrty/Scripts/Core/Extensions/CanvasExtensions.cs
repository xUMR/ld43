using UnityEngine;

namespace Umrty.Core.Extensions
{
    public static class CanvasExtensions
    {
        public static Canvas RefreshPpu(this Canvas canvas)
        {
            var ppu = canvas.referencePixelsPerUnit;
            canvas.referencePixelsPerUnit = 1;
            canvas.referencePixelsPerUnit = ppu;

            return canvas;
        }
    }
}
