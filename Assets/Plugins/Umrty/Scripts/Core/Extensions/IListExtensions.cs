using System;
using System.Collections.Generic;
using Random = UnityEngine.Random;

namespace Umrty.Core.Extensions
{
    public static class IListExtensions
    {
        public static void Shuffle<T>(this IList<T> source)
        {
            for (var i = 0; i < source.Count - 1; i++)
            {
                var j = Random.Range(i, source.Count);
                source.Swap(i, j);
            }
        }

        public static void Swap<T>(this IList<T> source, int i, int j)
        {
            var temp = source[i];
            source[i] = source[j];
            source[i] = temp;
        }

        public static IEnumerable<T> Loop<T>(this IList<T> source, int times)
        {
            for (int i = 0; i < times; i++)
            {
                for (var j = 0; j < source.Count; j++) yield return source[j];
            }
        }

        public static IEnumerable<T> Loop<T>(this IList<T> source)
        {
            while (true)
            {
                for (var j = 0; j < source.Count; j++) yield return source[j];
            }
        }

        public static IEnumerable<T> PingPong<T>(this IList<T> source, int times)
        {
            for (int i = 0; i < times; i++)
            {
                for (var j = 0; j < source.Count; j++) yield return source[j];
                for (var j = source.Count - 1; j >= 0; j--) yield return source[j];
            }
        }

        public static IEnumerable<T> PingPong<T>(this IList<T> source)
        {
            while (true)
            {
                for (var j = 0; j < source.Count; j++) yield return source[j];
                for (var j = source.Count - 1; j >= 0; j--) yield return source[j];
            }
        }

        public static void AddIf<T>(this IList<T> list, T item, Func<IList<T>, T, bool> predicate)
        {
            if (predicate(list, item))
                list.Add(item);
        }

        public static void AddIf<T>(this IList<T> list, T item, Func<T, bool> predicate)
        {
            if (predicate(item))
                list.Add(item);
        }

        public static void AddIfNotInList<T>(this IList<T> list, T item) =>
            AddIf(list, item, (lst, e) => !lst.Contains(e));
    }
}
