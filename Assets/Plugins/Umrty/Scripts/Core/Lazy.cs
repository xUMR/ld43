using System;

// ReSharper disable UnusedMember.Global

namespace Umrty.Core
{
    public sealed class Lazy<T> where T : UnityEngine.Object
    {
        private readonly Func<T> _initializer;

        private T _value;
        public T Value => _value ? _value : _value = _initializer();

        public Lazy(Func<T> initializer)
        {
            _initializer = initializer;
        }
    }
}
