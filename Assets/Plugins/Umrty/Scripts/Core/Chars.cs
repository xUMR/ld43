namespace Umrty.Core
{
    public static class Chars
    {
        public const string Digits = "0123456789";
        public const string DigitsPositive = "123456789";

        public const string AsciiLettersUpper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        public const string AsciiLettersLower = "abcdefghijklmnopqrstuvwxyz";

        public const string AsciiLetters = AsciiLettersUpper + AsciiLettersLower;
        public const string AsciiAlphaNumeric = Digits + AsciiLettersUpper + AsciiLettersLower;
    }
}
