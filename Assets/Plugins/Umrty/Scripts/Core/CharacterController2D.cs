using System.Runtime.InteropServices.WindowsRuntime;
using Umrty.Core.Extensions;
using Umrty.MagicStrings;
using UniRx;
using UnityEngine;

namespace Umrty.Core
{
    public class CharacterController2D : MonoBehaviour
    {
        public float Speed;

        [SerializeField] private Rigidbody2D _body;
        [SerializeField] private LayerMask _blockingLayer;
        [SerializeField] private float _blockDistance = 1;
        [SerializeField] private Transform _transformToMove;

        private void Start()
        {
            _body = _body == null ? GetComponentInChildren<Rigidbody2D>() : _body;

            if (_body == null || _body.isKinematic)
                TransformMovement();
            else
                RigidbodyMovement();
        }

        private void TransformMovement() =>
            InputManager.Axis2DStream[InputAxes.Horizontal, InputAxes.Vertical]
                .Where(v => !v.Approx(Vector2.zero))
                .Select(v => v.normalized * Time.deltaTime)
                .SubscribeWithState(this, (v, self) =>
                {
                    if (!self.enabled) return;

                    var movement = v * self.Speed;

                    var hitsX = Physics2D.RaycastNonAlloc(self._transformToMove.position, movement.JustX(),
                        Arrays<RaycastHit2D>.Single, self._blockDistance, self._blockingLayer);

                    var hitsY = Physics2D.RaycastNonAlloc(self._transformToMove.position, movement.JustY(),
                        Arrays<RaycastHit2D>.Single, self._blockDistance, self._blockingLayer);

                    if (hitsX > 0)
                        movement = movement.JustY();
                    if (hitsY > 0)
                        movement = movement.JustX();

                    self._transformToMove.Translate(movement, Space.World);
                })
                .AddTo(this);

        private void RigidbodyMovement() =>
            InputManager.Axis2DFixedStream[InputAxes.Horizontal, InputAxes.Vertical]
                .Select(v => v.normalized)
                .SubscribeWithState(this, (v, self) => self._body.velocity = v * self.Speed)
                .AddTo(this);
    }
}
