namespace Umrty.Core
{
    public static class Arrays<T>
    {
        // maybe use Enumerable.Empty instead
        public static readonly T[] Empty = new T[0];
        public static readonly T[] Single = new T[1];
    }
}
