// GENERATED //

namespace Umrty.MagicStrings
{
    public class InputAxes
    {
        public static readonly string Horizontal = "Horizontal";
        public static readonly string Vertical = "Vertical";
        public static readonly string RestartLevel = "RestartLevel";
        public static readonly string Fire = "Fire";
        public static readonly string Fire1 = "Fire1";
        public static readonly string Fire2 = "Fire2";
        public static readonly string MouseX = "Mouse X";
        public static readonly string MouseY = "Mouse Y";
        public static readonly string Horizontal1 = "Horizontal1";
        public static readonly string Vertical1 = "Vertical1";
        public static readonly string Horizontal2 = "Horizontal2";
        public static readonly string Vertical2 = "Vertical2";
        public static readonly string Submit = "Submit";
        public static readonly string Cancel = "Cancel";
        public static readonly string LookX = "LookX";
        public static readonly string LookY = "LookY";
        public static readonly string LookX1 = "LookX1";
        public static readonly string LookY1 = "LookY1";
        public static readonly string LookX2 = "LookX2";
        public static readonly string LookY2 = "LookY2";
        public static readonly string TargetLock = "TargetLock";
        public static readonly string TargetLock1 = "TargetLock1";
        public static readonly string SwapCharacter = "SwapCharacter";
        public static readonly string SwapCharacter1 = "SwapCharacter1";
        public static readonly string SwapCharacter2 = "SwapCharacter2";
    }
}
