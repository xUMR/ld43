// GENERATED //

using UnityEngine.SceneManagement;

namespace Umrty.MagicStrings
{
    public class Scenes
    {
        public static readonly string Coop = "Coop";
        public static readonly string Keys = "Keys";
        public static readonly string Menu = "Menu";
        public static readonly string Single = "Single";
        public static readonly string Single0 = "Single0";
        public static readonly string Single1 = "Single1";
        public static readonly string Single2 = "Single2";
        public static readonly string Test = "Test";
        public static readonly string Tutorial = "Tutorial";
        public static readonly string TutorialCont = "TutorialCont";
        public static readonly string TutorialOld = "TutorialOld";
    }
}
