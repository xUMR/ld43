// GENERATED //

using UnityEngine;

namespace Umrty.MagicStrings
{
    public class SortingLayers
    {
        public static readonly string Background = "Background";
        public static readonly string Default = "Default";
        public static readonly string Enemy = "Enemy";
        public static readonly string Player = "Player";
        public static readonly string Foreground = "Foreground";

        public static readonly int BackgroundId = SortingLayer.NameToID(Background);
        public static readonly int DefaultId = SortingLayer.NameToID(Default);
        public static readonly int EnemyId = SortingLayer.NameToID(Enemy);
        public static readonly int PlayerId = SortingLayer.NameToID(Player);
        public static readonly int ForegroundId = SortingLayer.NameToID(Foreground);

        public static readonly int BackgroundValue = SortingLayer.GetLayerValueFromID(BackgroundId);
        public static readonly int DefaultValue = SortingLayer.GetLayerValueFromID(DefaultId);
        public static readonly int EnemyValue = SortingLayer.GetLayerValueFromID(EnemyId);
        public static readonly int PlayerValue = SortingLayer.GetLayerValueFromID(PlayerId);
        public static readonly int ForegroundValue = SortingLayer.GetLayerValueFromID(ForegroundId);
    }
}
